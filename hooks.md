# Plugging in an SVG rendering library in FreeType to support OT-SVG fonts

The OT-SVG project has been designed in a way that allows users to plug any SVG rendering library, as long as the library meets the requirements of OT-SVG fonts. An external library is plugged in by the use of hooks or function pointers. The `ot-svg` rendering module stores four function pointers and calls them when needed. To plug in a library, you need to do two things:

1. Write the functions (hooks)
2. Set them

## Creating the hooks

Let us discuss the the hooks. There are four in total. Each hook has access to `FT_Library` either directly or through `slot->library`. The `FT_Library_Rec` has a field named `svg_renderer_state`. The hooks can create a structure of their own and set its address in `svg_renderer_state`. This can allow the hooks to persist state between different hook calls. 

#### Init Hook

```c
typedef FT_Error
(*SVG_Lib_Init_Func)( FT_Library  library );
```

At a first glance, one might think that this function is called when the `ot-svg` module is being initialized. But that is not true. This hook is called when the first OT-SVG glyph is rendered. This lazy loading is preferred so that no SVG specific initializations are executed unless SVG rendering is really needed. This hook should do any sort of initializations the SVG rendering library might require. For example, if state is needed, the state structure should be allocated by this hook.

#### Free Hook 

```c
typedef void
(*SVG_Lib_Free_Func)( FT_Library  library );
```

The free hook is quite self-explanatory. It's called when the module is being freed. It's only called if `init_hook` was called first. So, if no SVG glyphs are rendered, neither the `init_hook` nor the `free_hook` are called. Any sort of memory cleanups or library cleanups should be performed in the `free_hook`. 

#### Preset Hook

```c
typedef FT_Error
(*SVG_Lib_Preset_Slot_Func)( FT_GlyphSlot  slot, FT_Bool  cache);
```

This hook is supposed to preset the `slot`. Look at the behavior of `ft_glyphslot_preset_bitmap` for traditional outlines to see what presetting really means. This hook is be called from two places. Firstly from `ft_glyphslot_preset_bitmap` and secondly from `ft_svg_render` right before the render hook is called. When it's the former, `cache` is `FALSE` and when it's the latter, `cache` is `TRUE`. The reason for putting this `cache` variable is that for SVG rendering, most of the things which are calculated to preset the glyph are also needed while rendering the glyph. Thus, it would be to wise to just store these calculations and reuse them instead of doing them multiple times. When `cache` is `FALSE`, the function shouldn't store any data for reuse. However, when `cache` is `TRUE` it can store data in `state` because it's assured that the render hook will be called right after this function finishes executing. The preset hook should set the following values:

1. `bitmap_left`
2. `bitmap_top`
3. `bitmap.rows`
4. `bitmap.width`
5. `bitmap.pitch`
6. `bitmap.pixel_mode => FT_PIXEL_MODE_BGRA` 

#### Render Hook

```c
typedef FT_Error
(*SVG_Lib_Render_Func)( FT_GlyphSlot  slot );
```

The render hook is called right after preset hook has been called with `cache` set to `TRUE`. This hook is expected to render the glyph and place the resulting pixmap in `slot->bitmap.buffer`. Note that the memory for this pixmap buffer would be already allocated by FreeType, so don't allocate it inside the hook and don't change the value of `slot->bitmap.buffer`. The memory for the buffer is supposed to be allocated/freed by FreeType not by the hooks. After putting the data in the pixmap, the following fields must be set.

1. `slot->bitmap.num_grays = 256`
2. `slot->internal->flags |= FT_GLYPH_OWN_BITMAP`
3. `slot->format = FT_GLYPH_FORMAT_BITMAP`

#### *Important Note*

Both the preset hook and the render hook must take into account transformations. The next section should tell you where to find the data for the transformations. Something important to note is that the SVG coordinate system is different from TTF/CFF coordinate system. The `y` axis is inverted and the EM square can be of different units. Thus,  the hooks should expect the end user to give a transformation matrix that is designed for traditional outlines, then the hooks will  convert it into an equivalent one for the SVG coordinate system and then apply it. 

#### *Glyph data for presetting and rendering OT-SVG glyphs*

Most of the data needed for presetting and rendering the glyph can be found in `FT_SVG_DocumentRec` which is referenced in `slot->other`. The structure is:

```c
  typedef struct FT_SVG_DocumentRec_
  {
    FT_Byte*         svg_document;
    FT_ULong         svg_document_length;
    FT_Size_Metrics  metrics;
    FT_UShort        units_per_EM;
    FT_UShort        start_glyph_id;
    FT_UShort        end_glyph_id;
    FT_Matrix        transform;
    FT_Vector        delta;
  } FT_SVG_DocumentRec;
```

1. **svg_document**: Pointer to the SVG document. This string doesn't terminate with a null byte `'\0'`.
2. **svg_document_length:** Length of the SVG document string.
3. **metrics:** Contains size information. In this structure, `x_ppem` and `y_ppem` are the useful ones.
4. **units_per_EM:** It's the number of units in the EM square in TTF/CFF outlines. It's useful since if a document doesn't contain any `viewBox` or `width/height`, the EM square of SVG coordinate system is the same as that of traditional outlines.
5. **start_glyph_id & end_glyph_id:** These directly come from the SVG table. These fields gives the range of the glyph description that any particular glyph contains. Say if a document contains glyph descriptions from ID 9 to 13. `start_glyph_id` will be 9 and `end_glyph_id` will be 13;
6. **transform:** Contains the transformation matrix.
7. **delta:** Contains the translation information.

## Setting the hooks

Once you've written these hook functions, setting them is easy. Right after you initialize the library in your main program add the following code:

```c
SVG_RendererHooks  hooks;
hooks.init_svg        = (SVG_Lib_Init_Func)<name of your init function>;
hooks.free_svg        = (SVG_Lib_Free_Func)<name of your free function>;
hooks.render_svg      = (SVG_Lib_Render_Func)<name of your render function>;
hooks.preset_slot     = (SVG_Lib_Preset_Slot_Func)<name of your preset function>;
FT_Property_Set( library, "ot-svg", "svg_hooks", &hooks );
```

